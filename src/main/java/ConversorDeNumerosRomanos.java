import java.util.List;

public class ConversorDeNumerosRomanos {
    private int valorAlgarismosArabicos;
    private String valorAlgarismosRomanos;
    private AlgarismosRomanos algarismosRomanos;

    public ConversorDeNumerosRomanos() {
        valorAlgarismosRomanos = "";
    }

    public String conversorDeNumero(int numero){
        if(numero<1 || numero >3999){
            throw new RuntimeException("Número Invalido!\nInsira um valor entre (1) e (3999)");
        }

        int valorDoAlgarismo;

        valorDoAlgarismo = numero/1000;
        for (int i = 0; i < valorDoAlgarismo; i++) {
            valorAlgarismosRomanos+= AlgarismosRomanos.getAlgarismosRomanos(1000).name();
        }
        numero-=valorDoAlgarismo*1000;

        if(numero >= 900) {
            numero -= 900;
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(100).name();
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(1000).name();
        }
        if(numero >= 500) {
            numero -= 500;
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(500).name();
        }
        if(numero >= 400) {
            numero -= 400;
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(100).name();
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(500).name();
        }

        valorDoAlgarismo = numero/100;
        for (int i = 0; i < valorDoAlgarismo; i++) {
            valorAlgarismosRomanos+= AlgarismosRomanos.getAlgarismosRomanos(100).name();
        }
        numero-=valorDoAlgarismo*100;

        if(numero >= 90) {
            numero -= 90;
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(10).name();
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(100).name();
        }
        if(numero >= 50) {
            numero -= 50;
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(50).name();
        }
        if(numero >= 40) {
            numero -= 40;
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(10).name();
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(50).name();
        }

        valorDoAlgarismo = numero/10;
        for (int i = 0; i < valorDoAlgarismo; i++) {
            valorAlgarismosRomanos+= AlgarismosRomanos.getAlgarismosRomanos(10).name();
        }
        numero-=valorDoAlgarismo*10;

        if(numero == 9) {
            numero -= 9;
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(1).name();
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(10).name();
        }
        if(numero >= 5) {
            numero -= 5;
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(5).name();
        }
        if(numero == 4) {
            numero -= 4;
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(1).name();
            valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(5).name();
        }
        for (int i = 0; i < numero; i++) {
           valorAlgarismosRomanos+=AlgarismosRomanos.getAlgarismosRomanos(1).name();
        }

        return valorAlgarismosRomanos;
        }

}
