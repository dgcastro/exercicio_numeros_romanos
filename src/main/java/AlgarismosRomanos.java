public enum AlgarismosRomanos {
    I(1), V(5), X(10), L(50), C(100), D(500), M(1000);
    private final int valor;

    private AlgarismosRomanos(int valor){
        this.valor = valor;
    }
    public int getValue(){
        return valor;
    }
    public static AlgarismosRomanos getAlgarismosRomanos(int valor){
        for (AlgarismosRomanos algarismoRomano: AlgarismosRomanos.values()
             ) {
            if(algarismoRomano.getValue() == valor){
                return algarismoRomano;
            }
        }
        return null;
    }
}
