import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TesteConversorDeNumerosRomanos {
    private ConversorDeNumerosRomanos conversorDeNumerosRomanos;

    @BeforeEach
    public void setUp(){
        this.conversorDeNumerosRomanos = new ConversorDeNumerosRomanos();
    }

    @Test
    void testValidarRangeMinimo(){
        Assertions.assertThrows(RuntimeException.class, () -> {conversorDeNumerosRomanos.conversorDeNumero(0);});
    }

    @Test
    void testValidarRangeMaximo(){
        Assertions.assertThrows(RuntimeException.class, () -> {conversorDeNumerosRomanos.conversorDeNumero(4000);});
    }


    @Test
    void testConversorLimite(){
        String numeroRomano = "MMMCMXCIX";
        int numeroArabe = 3999;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorMil(){
        String numeroRomano = "M";
        int numeroArabe = 1000;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorNovecentos(){
        String numeroRomano = "CM";
        int numeroArabe = 900;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorQuinhentos(){
        String numeroRomano = "D";
        int numeroArabe = 500;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorQuatrocentos(){
        String numeroRomano = "CD";
        int numeroArabe = 400;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorCem(){
        String numeroRomano = "C";
        int numeroArabe = 100;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorNoventa(){
        String numeroRomano = "XC";
        int numeroArabe = 90;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorCinquenta(){
        String numeroRomano = "L";
        int numeroArabe = 50;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorQuarenta(){
        String numeroRomano = "XL";
        int numeroArabe = 40;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorDez(){
        String numeroRomano = "X";
        int numeroArabe = 10;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorNove(){
        String numeroRomano = "IX";
        int numeroArabe = 9;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorCinco(){
        String numeroRomano = "V";
        int numeroArabe = 5;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorQuatro(){
        String numeroRomano = "IV";
        int numeroArabe = 4;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }

    @Test
    void testConversorUm(){
        String numeroRomano = "I";
        int numeroArabe = 1;
        Assertions.assertEquals(numeroRomano,conversorDeNumerosRomanos.conversorDeNumero(numeroArabe));
    }
}
